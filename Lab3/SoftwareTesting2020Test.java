import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SoftwareTesting2020Test {

    SoftwareTesting2020 classroom = new SoftwareTesting2020();
    @Mock
    private Date mock_date;
    @Mock
    private Hospital mock_hospital;
    @Spy
    private Hospital spy_hospital;
    @Mock
    private NCTUDatabase mock_database;

    @org.junit.jupiter.api.Test
    void test_a() throws InterruptedException {
        Student s = new Student("ID1", 38);
        when(mock_date.getWeekday()).thenReturn(4);  // Wednesday
        classroom.date = mock_date;
        classroom.hospital = mock_hospital;
        classroom.enterClass(s);
        verify(mock_hospital, never()).treatment(s);
    }

    @org.junit.jupiter.api.Test
    void test_b() throws InterruptedException {
        Student s = new Student("ID2", 38);
        when(mock_date.getWeekday()).thenReturn(5);  // Thursday
        doNothing().when(spy_hospital).isolation(s);
        classroom.date = mock_date;
        classroom.hospital = spy_hospital;
        assertEquals("After a long time treatment. The student can get out! ^__^", classroom.enterClass(s));
    }

    @org.junit.jupiter.api.Test
    void test_c() throws InterruptedException {
        Student s = new Student("ID3", 38);
        Student s1 = new Student("ID4", 38);
        Student s2 = new Student("ID5", 38);
        when(mock_date.getWeekday()).thenReturn(5);  // Thursday
        doNothing().when(spy_hospital).isolation(s);
        doNothing().when(spy_hospital).isolation(s1);
        doNothing().when(spy_hospital).isolation(s2);
        classroom.date = mock_date;
        classroom.hospital = spy_hospital;
        classroom.enterClass(s);
        classroom.enterClass(s1);
        classroom.enterClass(s2);
        ArrayList ans = new ArrayList();
        ans.add("ID3");
        ans.add("ID4");
        ans.add("ID5");
        assertEquals(ans ,classroom.hospital.getLog());
    }

    @org.junit.jupiter.api.Test
    void test_d() {
        String student_id = "ID6";
        when(mock_database.getScore(student_id)).thenReturn(87);
        classroom.MyDatabase = mock_database;
        assertEquals(classroom.getScore(student_id), 87);
    }

    @org.junit.jupiter.api.Test
    void test_e() {
        paypalServiceFake paypal = new paypalServiceFake();
        String result = classroom.donate(paypal);
        assertEquals(result, "Thanks you");
    }
}

final class paypalServiceFake implements paypalService{
    @Override
    public String doDonate() {
        return "succeed";
    }
}
