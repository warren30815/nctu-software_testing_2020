import static org.junit.jupiter.api.Assertions.*;

class VehicleTest {

    @org.junit.jupiter.api.Test
    void setSpeed() {
        Vehicle v1 = new Vehicle();
        v1.setSpeed(5);
        assertEquals(5, v1.getSpeed());
    }

    @org.junit.jupiter.api.Test
    void setDir() {
        Vehicle v1 = new Vehicle();
        v1.setDir("south");
        assertEquals("south", v1.getDir());
    }

    @org.junit.jupiter.api.Test
    void getSpeed() {
        Vehicle v2 = new Vehicle(5, "south");
        assertEquals(5, v2.getSpeed());
    }

    @org.junit.jupiter.api.Test
    void getDir() {
        Vehicle v2 = new Vehicle(5, "south");
        assertEquals("south", v2.getDir());
    }

    @org.junit.jupiter.api.Test
    void totalVehicle() {
        Vehicle v1 = new Vehicle();
        assertNotEquals(-1,v1.totalVehicle());
    }
}